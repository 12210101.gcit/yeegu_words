// Function to handle form submission
function handleFormSubmit(event) {
	event.preventDefault(); // Prevent form submission

	// Get the selected file from the input
	const fileInput = document.getElementById("photoInput");
	const file = fileInput.files[0];

	if (file) {
		// Create a FileReader object to read the file
		const reader = new FileReader();

		// Define a callback function to be executed when the file is loaded
		reader.onload = function (e) {
			const imageSrc = e.target.result;

			// Make the prediction request using the image source
			makePrediction(imageSrc);
		};

		// Read the file as Data URL
		reader.readAsDataURL(file);
	}
}

// Function to make the prediction request
function makePrediction(imageSrc) {
	// Clear previous prediction result
	document.getElementById("predictionResult").innerHTML = "";

	// Here, you can use your font prediction model or API
	// and make the prediction request using imageSrc

	// Mock prediction result for demonstration purposes
	const predictedValue = "JooYig";

	// Display the prediction result
	const predictionContainer = document.getElementById("predictionResult");
	const predictionResult = document.createElement("h3");
	predictionResult.textContent = predictedValue;
	predictionContainer.appendChild(predictionResult);

	// Show the corresponding Predict container and hide others
	const jooyigContainer = document.getElementById("jooyig-container");
	const tsuyikContainer = document.getElementById("tsuyik-container");

	if (predictedValue === "JooYig") {
		jooyigContainer.style.display = "block";
		jooyigContainer.classList.remove("col-md-6");
		jooyigContainer.classList.add("col-md-12");
		tsuyikContainer.style.display = "none";
		jooyigContainer.style.textAlign = "center";
	} else if (predictedValue === "TsuYik") {
		tsuyikContainer.style.display = "block";
		tsuyikContainer.classList.remove("col-md-6");
		tsuyikContainer.classList.add("col-md-12");
		jooyigContainer.style.display = "none";
		tsuyikContainer.style.textAlign = "center";
	} else {
		jooyigContainer.style.display = "none";
		tsuyikContainer.style.display = "none";
	}
}

// Attach event listener to the form submit event
const form = document.getElementById("uploadForm");
form.addEventListener("submit", handleFormSubmit);
