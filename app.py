from flask import Flask, render_template , request
import cv2
import joblib
import os
from flask_session import Session





app = Flask(__name__)

app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)
model = joblib.load("font_pick.joblib")

CATERGORIES = ["DCC_UCHEN", "DDC_JOYIG"]


@app.route("/predict", methods=['POST'])
def prediction():
    if request.method == 'POST':
        image_file = request.files['image']
        temp_path = 'temp.jpg'
        image_file.save(temp_path)

        unseen_image = cv2.imread(temp_path, cv2.IMREAD_GRAYSCALE)
        resized_unseen_image = cv2.resize(unseen_image, (120, 120))
        reshaped_unseen_image = resized_unseen_image.flatten().reshape(1, -1)
        predicted_label = model.predict(reshaped_unseen_image)


        result = CATERGORIES[predicted_label[0]]
       

        

        os.remove(temp_path)
        if result == 'DCC_UCHEN' :
            return render_template('Tsuyik.html')
        else :
            return render_template("joyig.html" )
    
@app.route("/")
def index():
    return render_template("index.html")

if __name__=="__main__":
    app.run(debug=True)